package cmir2070MV;

import cmir2070MV.controller.EmployeeController;
import cmir2070MV.exception.EmployeeException;
import cmir2070MV.model.DidacticFunction;
import cmir2070MV.model.Employee;
import cmir2070MV.repository.fileRepository.EmployeeFileRepository;
import cmir2070MV.repository.interfaces.EmployeeRepositoryInterface;
import cmir2070MV.validator.EmployeeValidator;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class ModifyEmployeeTest {

    private EmployeeRepositoryInterface employeeRepository;
    private EmployeeController controller;
    private EmployeeValidator employeeValidator;

    @Before
    public void setUp() {
        employeeRepository = new EmployeeFileRepository("employeesTest.txt");
        employeeValidator  = new EmployeeValidator();
        controller         = new EmployeeController(employeeRepository, employeeValidator);
    }

    @Test(expected = EmployeeException.class)
    public void testWBT01() throws EmployeeException {
        EmployeeRepositoryInterface wrongRepository = new EmployeeFileRepository("");
        EmployeeController wrongController = new EmployeeController(wrongRepository, employeeValidator);

        Employee oldEmployee = new Employee("Marius", "1971004013642", DidacticFunction.ASISTENT, "1020");
        Employee newEmployee = new Employee("Marius", "1971004013642", DidacticFunction.CONFERENTIAR, "1020");

        wrongController.modifyEmployee(oldEmployee, newEmployee);
    }

    @Test
    public void testWBT02() throws EmployeeException {
        Employee oldEmployee = new Employee("Marius", "1971004013642", DidacticFunction.ASISTENT, "1020");
        Employee newEmployee = new Employee("Marius", "1971004013642", DidacticFunction.CONFERENTIAR, "1020");

        controller.modifyEmployee(oldEmployee, newEmployee);

        List<Employee> all = employeeRepository.getEmployeeList();

        for (Employee e: all) {
            if (e.getCnp().equals(oldEmployee.getCnp())) {
                assertEquals(e.getFunction(), newEmployee.getFunction());
            }
        }

        controller.modifyEmployee(newEmployee, oldEmployee);

        all = employeeRepository.getEmployeeList();

        for (Employee e: all) {
            if (e.getCnp().equals(newEmployee.getCnp())) {
                assertEquals(e.getFunction(), oldEmployee.getFunction());
            }
        }
    }


}
