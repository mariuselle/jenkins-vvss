package cmir2070MV;



import cmir2070MV.controller.EmployeeController;
import cmir2070MV.exception.EmployeeException;
import cmir2070MV.model.DidacticFunction;
import cmir2070MV.model.Employee;
import cmir2070MV.repository.fileRepository.EmployeeFileRepository;
import cmir2070MV.repository.interfaces.EmployeeRepositoryInterface;
import cmir2070MV.validator.EmployeeValidator;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class TopDownTest {

    private EmployeeRepositoryInterface employeeRepository;
    private EmployeeController controller;
    private EmployeeValidator employeeValidator;

    @Before
    public void setUp() {
        employeeRepository = new EmployeeFileRepository("employeesTest.txt");
        employeeValidator  = new EmployeeValidator();
        controller         = new EmployeeController(employeeRepository, employeeValidator);
    }
    @Test
    public void test1() throws EmployeeException {
        int oldSize = controller.getEmployeesList().size();
        controller.addEmployee("Nume", "1123456789123", DidacticFunction.ASISTENT, "1020");
        int newSize = controller.getEmployeesList().size();
        assertEquals(oldSize+1, newSize);
    }

    @Test
    public void test2() throws EmployeeException {
        Employee oldEmployee = new Employee("Marius", "1971004013642", DidacticFunction.ASISTENT, "1020");
        Employee newEmployee = new Employee("Marius", "1971004013642", DidacticFunction.CONFERENTIAR, "1020");

        controller.modifyEmployee(oldEmployee, newEmployee);

        List<Employee> all = employeeRepository.getEmployeeList();

        for (Employee e: all) {
            if (e.getCnp().equals(oldEmployee.getCnp())) {
                assertEquals(e.getFunction(), newEmployee.getFunction());
            }
        }
    }

    @Test
    public void test3() {
        List<Employee> sorted = controller.getEmployeeListSort();
        assertEquals("Pacuraru", sorted.get(0).getLastName());
    }

    @Test
    public void test4() throws EmployeeException {
        int oldSize = controller.getEmployeesList().size();
        controller.addEmployee("Nume", "1123456789123", DidacticFunction.ASISTENT, "1020");
        int newSize = controller.getEmployeesList().size();
        assertEquals(oldSize+1, newSize);


        Employee oldEmployee = new Employee("Marius", "1971004013642", DidacticFunction.ASISTENT, "1020");
        Employee newEmployee = new Employee("Marius", "1971004013642", DidacticFunction.CONFERENTIAR, "1020");

        controller.modifyEmployee(oldEmployee, newEmployee);

        List<Employee> all = employeeRepository.getEmployeeList();

        for (Employee e: all) {
            if (e.getCnp().equals(oldEmployee.getCnp())) {
                assertEquals(e.getFunction(), newEmployee.getFunction());
            }
        }
    }

    @Test
    public void test5() throws EmployeeException {
        int oldSize = controller.getEmployeesList().size();
        controller.addEmployee("Nume", "1991212013642", DidacticFunction.ASISTENT, "20");
        int newSize = controller.getEmployeesList().size();
        assertEquals(oldSize+1, newSize);


        Employee oldEmployee = new Employee("Marius", "1971004013642", DidacticFunction.ASISTENT, "1020");
        Employee newEmployee = new Employee("Marius", "1971004013642", DidacticFunction.CONFERENTIAR, "1020");

        controller.modifyEmployee(oldEmployee, newEmployee);

        List<Employee> all = employeeRepository.getEmployeeList();

        for (Employee e: all) {
            if (e.getCnp().equals(oldEmployee.getCnp())) {
                assertEquals(e.getFunction(), newEmployee.getFunction());
            }
        }

        List<Employee> sorted = controller.getEmployeeListSort();
        assertEquals("Pacuraru", sorted.get(0).getLastName());
    }
}
