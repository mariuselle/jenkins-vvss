package cmir2070MV.main;

import cmir2070MV.model.Employee;
import cmir2070MV.repository.fileRepository.EmployeeFileRepository;
import cmir2070MV.repository.interfaces.EmployeeRepositoryInterface;
import cmir2070MV.validator.EmployeeValidator;
import cmir2070MV.controller.EmployeeController;
import cmir2070MV.model.DidacticFunction;

//functionalitati
//F01.	 adaugarea unui nou angajat (nume, prenume, CNP, functia didactica, salariul de incadrare);
//F02.	 modificarea functiei didactice (asistent/lector/conferentiar/profesor) a unui angajat;
//F03.	 afisarea salariatilor ordonati descrescator dupa salariu si crescator dupa varsta (CNP).

public class StartApp {
	
	public static void main(String[] args) {
		
		EmployeeRepositoryInterface employeesRepository = new EmployeeFileRepository("employeesTest.txt");
		EmployeeValidator validator = new EmployeeValidator();
		EmployeeController employeeController = new EmployeeController(employeesRepository, validator);
		
		for(Employee _employee : employeeController.getEmployeesList())
			System.out.println(_employee.toString());
		System.out.println("-----------------------------------------");
				
		for(Employee _employee : employeeController.getEmployeeListSort())
			System.out.println(_employee.toString());
		
		System.out.println( validator.isValid(new Employee("LastName", "1234567894322", DidacticFunction.TEACHER, "3400")) );
	}

}
