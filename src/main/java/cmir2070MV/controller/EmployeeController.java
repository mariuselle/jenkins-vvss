package cmir2070MV.controller;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import cmir2070MV.exception.EmployeeException;
import cmir2070MV.model.DidacticFunction;
import cmir2070MV.model.Employee;
import cmir2070MV.repository.interfaces.EmployeeRepositoryInterface;
import cmir2070MV.validator.EmployeeValidator;

public class EmployeeController {
	
	private EmployeeRepositoryInterface employeeRepository;
	private EmployeeValidator employeeValidator = new EmployeeValidator();

	public EmployeeController(EmployeeRepositoryInterface employeeRepository, EmployeeValidator employeeValidator) {
		this.employeeRepository = employeeRepository;
		this.employeeValidator = employeeValidator;
	}
	
	public void addEmployee(String lastName, String cnp, DidacticFunction didacticFunction, String salary) throws EmployeeException {

		Employee employee = new Employee(lastName, cnp, didacticFunction, salary);
		if (employeeValidator.isValid(employee)) {
			employeeRepository.addEmployee(employee);
		} else throw new EmployeeException("Employer invalid!");
	}
	
	public List<Employee> getEmployeesList() {
		return employeeRepository.getEmployeeList();
	}
	
	public void modifyEmployee(Employee oldEmployee, Employee newEmployee) throws EmployeeException {

		if (employeeValidator.isValid(newEmployee)) {
			employeeRepository.modifyEmployee(oldEmployee, newEmployee);
		} else throw new EmployeeException("Employer invalid!");
	}

	public List<Employee> getEmployeeListSort() {
		List<Employee> all = employeeRepository.getEmployeeList();

		Collections.sort(all, new Comparator<Employee>() {
			@Override
			public int compare(Employee o1, Employee o2) {
				int ok = Integer.compare(Integer.parseInt(o2.getSalary()), Integer.parseInt(o1.getSalary()));
				if (ok == 0) {
					return isOlder(o1.getCnp(),o2.getCnp());
				}

				return ok;
			}
		});

		return all;
	}

	private int isOlder(String cnp, String cnp2) {
		int year1 = Integer.parseInt(cnp.substring(1,3));
		int year2 = Integer.parseInt(cnp2.substring(1,3));

		int month1 = Integer.parseInt(cnp.substring(4,5));
		int month2 = Integer.parseInt(cnp2.substring(4,5));

		int day1 = Integer.parseInt(cnp.substring(6,7));
		int day2 = Integer.parseInt(cnp2.substring(6,7));

		if (year1 > year2)
			return 1;
		if (year2 > year1)
			return -1;

		if (month1 > month2)
			return 1;
		if (month2 > month1)
			return -1;

		if (day1 > day2)
			return 1;
		if (day2 > day1)
			return 1;

		return 0;
	}

	public void deleteEmployee(Employee employee) {
		employeeRepository.deleteEmployee(employee);
	}
	
}
