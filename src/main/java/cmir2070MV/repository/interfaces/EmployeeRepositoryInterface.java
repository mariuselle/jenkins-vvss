package cmir2070MV.repository.interfaces;

import java.util.List;

import cmir2070MV.exception.EmployeeException;
import cmir2070MV.model.Employee;

public interface EmployeeRepositoryInterface {
	
	void addEmployee(Employee employee);
	void deleteEmployee(Employee employee);
	void modifyEmployee(Employee oldEmployee, Employee newEmployee) throws EmployeeException;
	List<Employee> getEmployeeList();
	List<Employee> getEmployeeByCriteria(String criteria);

}
